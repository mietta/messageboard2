package uk.ac.gcu.wbp2.lab.mietanaka.messageboard2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MessageBoard {
    private final String mBTitle;
    private List<Topic> topics;

    public MessageBoard(String title){
        mBTitle = title;
        topics = new ArrayList<Topic>();
    }

    public void addTopic(Topic tp){
        topics.add(tp);
    }

    public Topic getTopic(int n){
        return topics.get(n);
    }

    public void display(){
        System.out.println(mBTitle);
        System.out.println("-------");
        Iterator<Topic> it = topics.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }
    }
}
