package uk.ac.gcu.wbp2.lab.mietanaka.messageboard2;

public class Main {
    //test2//
    private MessageBoard mb;
    private MessageBoardMenu menu;

    public Main(String str){
        mb = new MessageBoard(str);
        menu = new MessageBoardMenu(mb);
    }

    public void start(){
        menu.displayMessageBoardMenu();
    }

    public static void main(String[] args) {
        try{
            Main main =  new Main("Message Board 1");
            main.start();
        }
        catch(Exception e){
            e.printStackTrace();
        }





    }
}