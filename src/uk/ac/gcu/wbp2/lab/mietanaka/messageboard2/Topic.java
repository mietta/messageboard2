package uk.ac.gcu.wbp2.lab.mietanaka.messageboard2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Topic {
    private String title;
    private List<Message> messages;

    public Topic(String tpctitle){
        title = tpctitle;
        messages = new ArrayList<Message>();
    }

    public String displayTopic(){
        StringBuilder b = new StringBuilder();
        b.append(title); b.append('\n');
        b.append("-----------\n");
        Iterator<Message> it = messages.iterator();
        while(it.hasNext()){
            b.append(it.next().displayMessage());
        }
        return b.toString();
    }

    public void addMessage(Message m){
        messages.add(m);
    }

    public String getTitle(){
        return title;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append("Title: "); b.append(title);b.append('\n');
        for (Message m : messages) {
            b.append(m.displayMessage());
        }
        return b.toString();
    }
}


