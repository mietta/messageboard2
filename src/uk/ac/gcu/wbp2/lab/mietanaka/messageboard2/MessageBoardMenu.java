package uk.ac.gcu.wbp2.lab.mietanaka.messageboard2;

import java.util.ArrayList;
import java.util.Scanner;

public class MessageBoardMenu {

    private MessageBoard currentBoard;
    private Topic currentTopic;

    public MessageBoardMenu(MessageBoard mb) {
        currentBoard = mb;
    }

    public void displayMessageBoardMenu() {

        currentBoard.display();

        int option = 0;
        Scanner keyboard = new Scanner(System.in);

        do {
            System.out.println("Main Menu");
            System.out.println("-------------");
            System.out.println("1. Add new topic");
            System.out.println("2. Select a topic to view and post to");
            System.out.println("3. leave the appllication");
            System.out.println("-------------");
            System.out.println("Enter your choice between 1 and 3");
            option = keyboard.nextInt();

            switch (option) {
                case 1:
                    System.out.println("Enter the title of your new topic:");
                    String t = keyboard.next();
                    System.out.println("title is:" + t);

                    currentBoard.addTopic(new Topic(t));
                    currentBoard.display();
                    break;

                case 2:

                    System.out.println("Enter the number of the topic you would like to read");
                    int choice = keyboard.nextInt();
                    Topic topic = currentBoard.getTopic(choice);
                    System.out.println("topic " + choice + " selected");
                    new TopicMenu(topic, keyboard).displayTopicMenu();
                    break;

                case 3: System.out.println("goodbye.");
                    System.exit(0);
                default:
                    System.out.println("Invalid option. Please enter one of the options above.");
                    displayMessageBoardMenu();
            }


        } while (option != 3);
    }
}

