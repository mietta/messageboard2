package uk.ac.gcu.wbp2.lab.mietanaka.messageboard2;

import java.sql.Date;

public class Message {
    private String subject;
    private String contents;
    private Date sentDate;
    private String author;

    public Message(String Subject, String Contents, Date SetDate, String Author){
        subject = Subject;
        contents = Contents;
        sentDate = SetDate;
        author = Author;
    }

    public String displayMessage(){
        StringBuilder b = new StringBuilder();
        b.append("Subject: "); b.append(subject); b.append('\n');
        b.append("From: "); b. append(author); b.append('\n');
        b.append(sentDate);b.append('\n');
        b.append(contents); b.append('\n');
        return b.toString();
    }
}

